package com.mastip.personaltodolist.utilities;

/**
 * Created by HateLogcatError on 8/29/2017.
 */

public class Constant {

    public static final String TAG_USERNAME = "username";

    public static final String DB_NAME = "todolist";
    public static final String TABLE_TASK = "tb_task";
    public static final String TABLE_LOGIN = "tb_login";

    public static final String TAG_ID = "id";
    public static final String TAG_ID_USER = "id_user";
    public static final String TAG_TASK = "task";
    public static final String TAG_SUBMISSION = "submission";
    public static final String TAG_CREATED_AT = "created_at";
    public static final String TAG_FINISH = "finished";
    public static final String TAG_PRIORITY = "priority";
    public static final String TAG_STATUS = "status";
    public static final String TAG_PASSWORD = "password";

    public static final String TAG_CREATE_TABLE = "CREATE TABLE ";
    public static final String TAG_OPEN = "(";
    public static final String TAG_CLOSE = ")";
    public static final String TAG_DROP_TABLE = "DROP TABLE IF EXISTS ";

    public static final int TAG_CALENDAR = 999;

    public static final String TAG_SHARED_PREFERENCE = "TODOLIST";

    public static final int VIEW_HEADER = 0;
    public static final int VIEW_BODY_UNFINISH = 1;
    public static final int VIEW_BODY_FINISH = 2;
    public static final int NO_VIEW = 3;

    public static final int TAG_UNFINISH_ADDTASK = 1;
    public static final int TAG_FINISH_ADDTASK = 2;

    public static final int TAG_LOW_PRIORITY = 1;
    public static final int TAG_MEDIUM_PRIORITY = 2;
    public static final int TAG_HIGH_PRIORITY = 3;

    public static final int TAG_ACTIVITY_RESULT_ADDTASK = 1;

    public static final String TAG_ADD_TASK_RESULT = "addtaskresult";

    public static final int FLAG_DATE_INCREMENT = 3;

    public static final String TAG_MY_TASK = "My Task - ";
    public static final String TAG_EXPIRED = "Expired Task";
    public static final String TAG_MY_TASK_FINISH = "Task Finished";
}
